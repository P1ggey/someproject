from unicodedata import name
from django.contrib import admin
from .models import Item

admin.site.register(Item)