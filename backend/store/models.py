from django.db import models

class Item(models.Model):
	title: str = models.CharField(max_length=30)
	description: str = models.TextField(blank=True)
	type: int = models.ForeignKey('Type', on_delete=models.PROTECT, null=True)

	def __str__(self) -> str:
		return self.title

class Type(models.Model):
	name: str = models.CharField(max_length=30)

	def __str__(self) -> str:
		return self.name